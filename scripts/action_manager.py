#!/usr/bin/env python

import rospy
import sys, select, tty, termios
from std_msgs.msg        import String
from geometry_msgs.msg   import Twist
from sensor_msgs.msg     import JointState
from brics_actuator.msg  import JointPositions
from brics_actuator.msg  import JointVelocities
from brics_actuator.msg  import JointValue


class action_manager():

    def __init__( self ):

        self.receivedAction    = "5"
        self.currentPosition   = [ 0.0, 0.0, 0.0, 0.0, 0.0 ]
        self.currentVelocity   = [ 0.0, 0.0, 0.0, 0.0, 0.0 ]

        self.pub_base          = rospy.Publisher( '/cmd_vel', Twist, latch = True, queue_size = 2 )
        self.armPosPublisher   = rospy.Publisher( '/arm_1/arm_controller/position_command', JointPositions,  queue_size = 2 )
        self.armVelPublisher   = rospy.Publisher( '/arm_1/arm_controller/velocity_command', JointVelocities, queue_size = 2 )
        self.jointStateUpdater = rospy.Subscriber( 'joint_states', JointState, self.getJointState )
        self.dqnActuator       = rospy.Subscriber( 'dqn_cmd', String, self.dqnAction )
        self.manualActuator    = rospy.Subscriber( 'keys',    String, self.manualAction)

        self.positionBinding = {

                        "1" : [  0.01 ,  0.01 ],                    # arm_joint3 up,   arm_joint4 up
                        "2" : [  0.01 ,  0.0  ],                    # arm_joint3 up,   arm_joint4 stop
                        "3" : [  0.01 , -0.01 ],                    # arm_joint3 up,   arm_joint4 down
                        "4" : [  0.0  ,  0.01 ],                    # arm_joint3 stop, arm_joint4 up
#                       "5" : [  0.0  ,  0.0  ],                    # arm_joint3 stop, arm_joint4 stop, no need to publish
                        "6" : [  0.0  , -0.01 ],                    # arm_joint3 stop, arm_joint4 down
                        "7" : [ -0.01 ,  0.01 ],                    # arm_joint3 down, arm_joint4 up
                        "8" : [ -0.01 ,  0.0  ],                    # arm_joint3 down, arm_joint4 stop
                        "9" : [ -0.01 , -0.01 ],                    # arm_joint3 down, arm_joint4 down

                               }

        self.velocityBinding = {

                        "q" : [  0.02,  0.0,  0.0,  0.0,  0.0 ],    # arm_joint1 accelerate
                        "a" : [ -0.02,  0.0,  0.0,  0.0,  0.0 ],    # arm_joint1 decelerate
                        "w" : [  0.0,  0.02,  0.0,  0.0,  0.0 ],    # arm_joint2 accelerate
                        "s" : [  0.0, -0.02,  0.0,  0.0,  0.0 ],    # arm_joint2 decelerate
                        "e" : [  0.0,  0.0,  0.02,  0.0,  0.0 ],    # arm_joint3 accelerate
                        "d" : [  0.0,  0.0, -0.02,  0.0,  0.0 ],    # arm_joint3 decelerate
                        "r" : [  0.0,  0.0,  0.0,  0.02,  0.0 ],    # arm_joint4 accelerate
                        "f" : [  0.0,  0.0,  0.0, -0.02,  0.0 ],    # arm_joint4 decelerate
                        "t" : [  0.0,  0.0,  0.0,  0.0,  0.02 ],    # arm_joint5 accelerate
                        "g" : [  0.0,  0.0,  0.0,  0.0, -0.02 ],    # arm_joint5 decelerate


                               }





    def dqnAction( self, msg ):
        # 1. store latest action command
        self.receivedAction = msg.data[ 0 ]   #!!!!! unecessary step?

        # 2. 20 action per second(max frequency)
        rate = rospy.Rate( 20 )

        # 3. publish new position message according to received action
        if self.receivedAction != "0":

            posMsg = self.positionMsg()

            # 'don't move' case
            if self.receivedAction == "5":
                rate.sleep()
                return

            # 'move' case: current position + stepsize
            elif self.positionBinding.has_key( self.receivedAction ):
                posMsg.positions[ 2 ].value = self.currentPosition[ 2 ] + self.positionBinding[ self.receivedAction ][ 0 ] * 0.3
                posMsg.positions[ 3 ].value = self.currentPosition[ 3 ] + self.positionBinding[ self.receivedAction ][ 1 ] * 0.3

            self.armPosPublisher.publish( posMsg )
            rate.sleep()


    def manualAction( self, msg ):
        rate = rospy.Rate( 20 )
        if msg.data[ 0 ] != "0":

            velMsg = self.velocityMsg()

            if self.velocityBinding.has_key( msg.data[ 0 ] ):
                velUpdate = self.velocityBinding[msg.data[0]]
                for i in range(0,5):
                    velMsg.velocities[ i ].value = self.currentVelocity[ i ] + velUpdate[ i ]
                self.armVelPublisher.publish(velMsg)
            elif msg.data[ 0 ] == "c":
                self.armVelPublisher.publish(velMsg)

      
    # keep position and velocity values up to date
    def getJointState( self, msg ):
        if len( msg.name ) == 5:
            self.currentPosition = msg.position
            self.currentVelocity = msg.velocity

    # return an position message based on current joint position values
    def positionMsg( self ):
        message = JointPositions()
        message.positions = list()
        pos = [ JointValue(), JointValue(), JointValue(), JointValue(), JointValue() ]
        for i in range( 0, 5 ):
            pos[ i ].joint_uri = "arm_joint_" + str( i + 1 )
            pos[ i ].value     = self.currentPosition[ i ]
            pos[ i ].unit      = "rad"
            message.positions.append( pos[ i ] )
        return message

    # return an 0 velocity message
    def velocityMsg( self ):
        message = JointVelocities()
        message.velocities = list()
        vel = [ JointValue(), JointValue(), JointValue(), JointValue(), JointValue() ]
        for i in range(0,5):
            vel[ i ].joint_uri = "arm_joint_" + str( i + 1 )
            vel[ i ].value     = 0.0
            vel[ i ].unit      = "s^-1 rad"
            message.velocities.append( vel[ i ] )
        return message


if __name__ == '__main__':
    rospy.init_node( 'action_manager', anonymous = True )
    actionManager = action_manager()
    rospy.spin()
