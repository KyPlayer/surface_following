#!/usr/bin/env python
from __future__          import division
import cv2
import copy
import rospy
import numpy as np
import replay
import random
import pickle
import os.path


from keras               import optimizers, losses
from keras.models        import Model, Sequential, save_model, load_model
from keras.layers        import Input, Convolution2D, Convolution3D, Dense, Activation, Flatten, concatenate
from keras.optimizers    import SGD , Adam
from std_msgs.msg        import String
from geometry_msgs.msg   import Twist
from sensor_msgs.msg     import Image
from sensor_msgs.msg     import JointState
from brics_actuator.msg  import JointPositions
from brics_actuator.msg  import JointValue
from brics_actuator.msg  import JointVelocities
from cv_bridge           import CvBridge, CvBridgeError


GAMMA = 0.95



class deepQLearning():

    def __init__( self ):
        self.imageArray        = None
        self.jointPosition     = [ 0.0, 0.0, 0.0, 0.0, 0.0 ]  
        self.jointVelocity     = [ 0.0, 0.0, 0.0, 0.0, 0.0 ]
        self.action            = [ '5' ]
#        self.keyboardAction    = [ '0' ]
        self.rewardList        = list()
        self.terminalList      = list()            # !!!!! ternimal condition not defined
        self.goodContact       = False
        self.contactRate       = 0.0
        self.epochs            = 0
        self.iterations        = 0


        self.bridge            = CvBridge()
        self.imageProcessor    = rospy.Subscriber( 'image_raw_republish', Image,      self.imageToArray )
        self.jointStateUpdater = rospy.Subscriber( 'joint_states',        JointState, self.getJointState ) 
#        self.keyboardupdater   = rospy.Subscriber( 'keys',                String,     self.getKeyboardAction )
        self.actionPublisher   = rospy.Publisher(  'dqn_cmd',             String,     queue_size = 1, latch = True )



    # turn image into numpy.ndarray
    def imageToArray( self, img ):
            cv_image     = self.bridge.imgmsg_to_cv2( img, "bgr8" )
            cv_image     = cv2.cvtColor( cv_image, cv2.COLOR_BGR2GRAY )       
            resizedImage = cv2.resize( cv_image, ( 64, 48 ) )

#            cv2.imshow("Image window", cv_image)
#            cv2.imwrite('/home/chen/catkin_ws/src/surface_following/scripts/back_ground_resized.png',resizedImage)    # !!!!! comment it out after first run

            self.imageArray = resizedImage

            background = cv2.imread('/home/chen/catkin_ws/src/surface_following/scripts/back_ground_resized.png',0)

            contactArea = cv2.subtract( resizedImage, background )
            ret,contactArea = cv2.threshold( contactArea, 20, 255, 0 )
            cv2.imshow( "Image window", contactArea )
            cv2.waitKey( 3 )

            contactPixels = len( np.nonzero( contactArea )[ 0 ] )
            self.contactRate = contactPixels / contactArea.size * 1000
            
#            print('contact rate: ' + str(self.contactRate))

#   !!!!! function can be removed
#    def getKeyboardAction(self,msg):
#        if int(msg.data[0]) in [0,1,2,3,4,5,6,7,8]:
#        self.keyboardAction = msg.data[0]

    # prepare input for the neural network 
    def preprocess( self, image, position, velocity, action ):

        image = np.expand_dims( image, axis = 0 )
        image = np.expand_dims( image, axis = 3 )

        position = list( position )
        joint_state_array = np.array( position + velocity )
        joint_state_array = np.expand_dims( joint_state_array, axis = 0 )

        action = np.expand_dims( action, axis = 0 )

        return [ image, joint_state_array, action ]

    # store joint postion and velocity value in variable"jointPosition" and "jointVelocity"
    def getJointState( self, jointState ):
        if len( jointState.name ) == 5:        # change to 5 on real youBot
            self.jointPosition = jointState.position
            self.jointvelocity = jointState.velocity

#            pos = list()
#            vel = list()
#            for p in self.jointPosition:
#                pos.append('%.2f' % p)
#            print( 'current position:  ', pos )
#            for v in self.jointVelocity:
#                vel.append('%.2f' % v)
#            print( 'current velocity:  ', vel )




    # get reward from sensor image
    def getReward( self, image ):
        try:
            background      = cv2.imread( '/home/chen/catkin_ws/src/surface_following/scripts/back_ground_resized.png', 0 )
            contactArea     = cv2.subtract( image, background )
            ret,contactArea = cv2.threshold( contactArea, 20, 255, 0 )

            contactPixels = len( np.nonzero( contactArea )[ 0 ] )
            contactRate   = contactPixels / contactArea.size * 1000

            if contactRate > 65:
                return int( -10 )
            elif contactRate <= 65 and contactRate > 25:
                return int( 10 )
            elif contactRate <= 25:
                return int( -10 )
        except:
#                print('unexpected error')
                return int( 0 )

    # !!!!!  to be check
    def epsilonGreedy( self, qList, epsilon ):
        threshold = random.random()
        if threshold >= epsilon:
            return np.argmax( np.array( qList ) ) + 1
        else:
            return random.randint( 1, 9 )


    def getQList( self, model, img, pos, vel ):
        qList = list()
        for actIndex in range( 0, 9 ):
            actionInput = [ 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
            actionInput[ actIndex ] = 1
            jointInput  = self.preprocess( img, pos, vel, actionInput )
            actionValue = model.predict( jointInput )
            qList.append( actionValue[ 0 ][ 0 ] )
        return qList

    def createDQN( self ):
        image_input = Input( shape = ( 48, 64, 1 ) )
        x = Convolution2D(  8, ( 4, 4 ), data_format = "channels_last", input_shape = ( 48, 64, 1 ), activation = 'relu' )( image_input ) #,activation='relu'
        x = Convolution2D( 16, ( 3, 3 ), activation  = 'relu' )( x )
        image_output = Flatten()( x )

        joint_state_input  = Input( shape = ( 10, ) )
        joint_state_output = Dense( 9 )( joint_state_input )

        action_input  = Input( shape = ( 9, ) )    # !!!!!
        action_output = Dense( 9 )( action_input )
       

        x       = concatenate( [ image_output, joint_state_output, action_output ] )

        x       = Dense( 9 )( x ) #  relu ?
        qValue  = Dense( 1 )( x )     #output a single value
        network = Model( inputs = [ image_input, joint_state_input, action_input ], outputs=qValue )
        print( "DQN build successfully" )
        return network

    def actionToVector( self, act ):
        action = [ 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
        action[ int( act ) - 1 ] = 1
        return action


# main function
if __name__ == '__main__':


# 1. initiate ros subscriber
    rospy.init_node( 'deep_reinforcement_learner', anonymous = True )
    print( 'node initialized' )

# 2. create the network
    dqn = deepQLearning()

    sgd = optimizers.SGD(  lr = 0.0001, momentum = 0.0, decay = 0.0, nesterov = False )
    opt = optimizers.Adam( lr = 0.0001, beta_1 = 0.9, beta_2 = 0.999, epsilon = None, decay = 0.0, amsgrad = False )
    opt2 = optimizers.RMSprop( lr = 0.001, rho = 0.9, epsilon = 1e-06 )

    modelMain = dqn.createDQN()
    modelMain.compile( optimizer = opt2, loss = losses.mean_squared_error ) # double check here later
    if os.path.isfile( '/home/chen/catkin_ws/src/surface_following/observation/obs.pkl' ): 
        modelMain.load_weights( '/home/chen/catkin_ws/src/surface_following/model/weight.h5' )
    else:
        modelMain.save_weights( '/home/chen/catkin_ws/src/surface_following/model/weight.h5' )            


    modelTarget = dqn.createDQN()
    modelTarget.compile( optimizer = opt2, loss = losses.mean_squared_error ) # double check here later
    modelTarget.load_weights( '/home/chen/catkin_ws/src/surface_following/model/weight.h5' )

    print( 'models ready' )


# 3. prepare learning
    rate = rospy.Rate( 10 )  # should be even lower
    recorder = replay.replay()  # the replay object to handle observation problem
    if os.path.isfile( '/home/chen/catkin_ws/src/surface_following/observation/obs.pkl' ): 
        with open( '/home/chen/catkin_ws/src/surface_following/observation/obs.pkl', 'rb' ) as data:  # 'w' for writing, 'a' for appending
            try:
                recorder.observations = pickle.load( data )
            except EOFError:
                print( 'EOF error' ) # solution needed
    print( "recorder ready" )


    if os.path.isfile( '/home/chen/catkin_ws/src/surface_following/statistic/data.pkl' ): 
        with open( '/home/chen/catkin_ws/src/surface_following/statistic/data.pkl', 'r' ) as data:
            dqn.epochs, dqn.rewardList, dqn.terminalList = pickle.load( data )
    else:
        with open( '/home/chen/catkin_ws/src/surface_following/statistic/data.pkl', 'w+' ) as data:  # save 0,0,0
            pickle.dump( [ dqn.epochs, dqn.rewardList, dqn.terminalList ], data )
    print( 'current epoch: '+ str( dqn.epochs ) )

    rewardSum = 0
#    terminalCounter = 0
    weightUpdateCounter = 0

    # 4. learning loop
    while True: 

        dqn.iterations      += 1
        weightUpdateCounter += 1
        print( "Iteration: " + str( dqn.iterations ) )

        # (1) estimate q value and take action
        img = copy.deepcopy( dqn.imageArray )    # use deepcopy here
        pos = copy.deepcopy( dqn.jointPosition )  # convert a tuple to a list
        vel = copy.deepcopy( dqn.jointVelocity )

#        qValueList = modelMain.predict(dqn.preprocess(img,pos,vel))
        qValueList = dqn.getQList( modelMain, img, pos, vel )
        print( qValueList )
        
        action = [0, 0, 0, 0, 0, 0, 0, 0, 0 ]
#        if dqn.keyboardAction != ['0'] or (dqn.iterations % 10) < 7 :
        if ( dqn.iterations % 10 ) < 7 :
            print( "enter next command" )
            try:
                nextInput = int( raw_input() )
            except ValueError:
                nextInput = 5
            while not nextInput in [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]:
                print( "please enter an integer" )
                try:
                    nextInput = int( raw_input() )
                except ValueError:
                    nextInput = 5
            action = dqn.actionToVector( nextInput )
            
        else:
            action = dqn.actionToVector( dqn.epsilonGreedy( qValueList, 0.2 ) )

        # the actual command executed by youBot

        dqn.actionPublisher.publish( str( ( np.argmax( action ) + 1 ) ) )
        rate.sleep()




        # (2) get reward and enter next state
        print( 'current contact rate :' + str( dqn.contactRate ) )

        newimg = copy.deepcopy( dqn.imageArray )
        newpos = copy.deepcopy( dqn.jointPosition )
        newvel = copy.deepcopy( dqn.jointVelocity )
        reward = dqn.getReward( img )
        tLabel = False
        
        rewardSum += reward

        # (3) record observation
        print( 'action = ' + str( np.argmax( action ) + 1 ) + ', reward = ' + str( reward ) )
        recorder.addReplay( img, pos, vel, action, reward, newimg, newpos, newvel, tLabel )

        # (4) weight update
        print( 'replay size: '+ str( len( recorder.observations ) ) )
        if len( recorder.observations ) > 100:    #  start learning
            batch = recorder.getBatch( 10 ) # 50 ok?

            # prepare training data and target label

            imgArray        = list()
            jointStateArray = list()
            actArray        = list()
            label           = list()

            for instance in batch:

                imgOld, posOld, velOld, act, observed_reward, imgNew, posNew, velNew, tL = instance  # !!!!! remove unnecessary variable later
                imgArray.append( imgOld )
                jointStateArray.append( list( posOld ) + velOld )
                actArray.append( act )
                targetQList = list()
                for actIndex in range( 0, 9 ): #,1,2,3,4,5,6,7,8
                    actionVector  = [ 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
                    actionVector[ actIndex ] = 1
                    labelInput    = dqn.preprocess( imgNew, posNew, velNew, actionVector )
                    labelOutput   = modelTarget.predict( labelInput )
                    targetQList.append( labelOutput[ 0 ][ 0 ] )
                targetValue   = observed_reward + GAMMA * max( targetQList )
                label.append( targetValue )

            imgArray        = np.expand_dims( imgArray, axis = 3 )
            jointStateArray = np.array( jointStateArray )
            actArray        = np.array( actArray )
            trainingData = [ imgArray, jointStateArray, actArray ]

            modelMain.fit( trainingData, label, batch_size = len( batch ) )

            # update weight for second model every 20 iterations
            if weightUpdateCounter % 20 == 0:
                print( 'second neural network updated' )
                modelTarget.set_weights( modelMain.get_weights() )
                modelMain.save_weights( '/home/chen/catkin_ws/src/surface_following/model/weight.h5' )
                print( 'reward sum = '+ str( rewardSum ) )




        if dqn.iterations == 199:
            dqn.iterations = 0
            dqn.epochs += 1
            dqn.rewardList.append( rewardSum )
            rewardSum = 0
            print( 'rewardSum = ' + str( rewardSum ) + ' , current epoch: ' + str( dqn.epochs ) )
            with open( '/home/chen/catkin_ws/src/surface_following/observation/obs.pkl', 'a' ) as data:
                pickle.dump(recorder.observations,data)
            with open( '/home/chen/catkin_ws/src/surface_following/statistic/data.pkl', 'w+' ) as data:
                pickle.dump( [ dqn.epochs, dqn.rewardList, dqn.terminalList ], data )


    try:
        rospy.spin()
    except KeyboardInterrupt:
        print( "Shutting down" )
        cv2.destroyAllWindows()

