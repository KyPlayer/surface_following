import random
import pickle
import numpy as np




class replay():

    def __init__(self):
        self.size          = 10000 
        self.observations   = list()

    def addReplay(self, img, pos, vel, act, rwd, newimg, newpos, newvel, tLabel):
        if len(self.observations) < self.size:
            self.observations.append([img,pos,vel,act,rwd,newimg,newpos,newvel,tLabel])
        else:
            randomIndex = random.ranint( 0, len(self.observations) - 1)
            self.observations[randomIndex] = [img,pos,vel,act,rwd,newimg,newpos,newvel,tLabel]

    def getBatch(self,batchSize):
        if batchSize > len(self.observations):
            print('batch size is greater than number of observations')
            return
        else:
            batch = random.sample(self.observations, batchSize)
            return batch
